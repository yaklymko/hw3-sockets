const username = sessionStorage.getItem("username");

export function renderRoomsList(roomsList, roomsPage, socket) {
    let roomsWrapper = document.getElementById("rooms-wrapper");

    roomsWrapper.remove();
    roomsWrapper = document.createElement("div")
    roomsWrapper.id = 'rooms-wrapper';
    roomsPage.appendChild(roomsWrapper);

    roomsList.forEach(room => {

        const cardDiv = document.createElement("div");
        cardDiv.classList.add("card");

        const cardBody = document.createElement("div");
        cardBody.classList.add("card-body");

        const cardHeader = document.createElement("h5");
        cardHeader.classList.add("card-title");
        cardHeader.textContent = room.name;

        const cardText = document.createElement("p");
        cardText.classList.add("card-text");
        cardText.textContent = `Players: ${room.players.length || 0}
                                    Status: ${room.status}`;

        const cardButton = document.createElement("a");
        cardButton.classList.add("card-link");
        cardButton.href = "#";
        cardButton.textContent = "Join!";
        cardButton.addEventListener("click", event => {
            event.preventDefault();
            socket.emit("JOIN_ROOM", room.name);
        })

        cardBody.appendChild(cardHeader)
        cardBody.appendChild(cardText);
        cardBody.appendChild(cardButton);

        cardDiv.appendChild(cardBody);
        roomsWrapper.appendChild(cardDiv);
    });
}

export function renderRoomInfo(room, socket) {
    const playersList = document.getElementById("game-players-list");
    playersList.innerHTML = '';

    const readyButton = document.getElementById("game-ready-battle");

    setGameButtonsVisible(room.status === "lobby");

    room.players.forEach(player => {
        const listItem = document.createElement("li");

        listItem.textContent = player.username;

        if (player.username === username) {
            listItem.textContent += " (that's you)";

            if (room.status === "lobby") {
                readyButton.onclick = () => {
                    socket.emit("PLAYER_SET_READY", !player.ready)
                }
                if (player.ready) {
                    readyButton.classList.remove("btn-primary");
                    readyButton.classList.add("btn-secondary");
                    readyButton.textContent = "I'M NOT READY";
                } else {
                    readyButton.classList.remove("btn-secondary");
                    readyButton.classList.add("btn-primary");
                    readyButton.textContent = "I'M READY";
                }
            }
        }

        if (room.status === "lobby") {
            const readyLabel = document.createElement("span");
            readyLabel.classList.add("badge");
            if (player.ready) {
                readyLabel.classList.add("badge-success")
                readyLabel.textContent += " READY";
            } else {
                readyLabel.classList.add("badge-danger")
                readyLabel.textContent += " NOT READY";
            }
            listItem.appendChild(readyLabel);
        } else {
            const progressListItem = document.createElement("li");

            const progressBar = document.createElement("progress");

            progressBar.max = 100;
            const progressBarValue = player.progress ? Math.floor(player.progress * 100 / room.textLen) : 0;
            if (progressBarValue === 100) {
                listItem.classList.add("player-finisher");
            }
            progressBar.value = progressBarValue;

            progressListItem.appendChild(progressBar);
            playersList.appendChild(progressListItem);
        }

        playersList.appendChild(listItem);
    });

}

export function setPageRooms(roomsPage, gamePage) {
    roomsPage.classList.remove("display-none");
    gamePage.classList.add("display-none");
}

export function setPageGame(roomsPage, gamePage) {
    roomsPage.classList.add("display-none");
    gamePage.classList.remove("display-none");
}

export function renderPreGameTimer(seconds, timerBlock) {
    setTimerBlock(true)
    timerBlock.textContent = seconds;
}

export function setGameButtonsVisible(isVisible) {
    const buttonsBlock = document.getElementsByClassName("buttons-panel")[0];
    if (isVisible) {
        buttonsBlock.classList.remove("display-none");
    } else {
        buttonsBlock.classList.add("display-none");
    }
}

export function setTimerBlock(isVisible) {
    const buttonsBlock = document.getElementsByClassName("timer-block")[0];
    if (isVisible) {
        buttonsBlock.classList.remove("display-none");
    } else {
        buttonsBlock.classList.add("display-none");
    }
}

export function setTextBlock(isVisible) {
    const textBlock = document.getElementsByClassName("text-block")[0];
    if (isVisible) {
        textBlock.classList.remove("display-none");
    } else {
        textBlock.classList.add("display-none");
    }
}

export function createGameText(text = "") {
    const undoneTextSpan = document.querySelector(".text-block .text-undone");
    const doneTextSpan = document.querySelector(".text-block .text-done");
    const currCharSpan = document.querySelector(".text-block .curr-char");
    doneTextSpan.textContent = "";
    currCharSpan.textContent = text[0];
    undoneTextSpan.textContent = text.substring(1);

}

export function createFinishersModalText(finishersArr) {
    let str = '';
    finishersArr.forEach((p, i) => {
        str += `Player [${p}] took ${i + 1} place!\n`;
    })
    return str;
}

export function setGameTimer(isVisible, value = 0) {
    const timer = document.getElementById("game-timer");
    if (isVisible) {
        timer.classList.remove("display-none");
    } else {
        timer.classList.add("display-none");
    }
    timer.textContent = value;
}




