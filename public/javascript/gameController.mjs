export function getGameText(textId) {
    const getUrl = `/game/texts/${textId}`;

    return fetch(getUrl, {
        method: "GET",
    }).then(res => {
        return res.json();
    }).then(parsedRes => {
        return parsedRes.text;
    });
}

let clickHandler;


export function initKeyboardListeners(socket) {

    function onKeyUp(event) {
        const currCharSpan = document.querySelector(".text-block .curr-char");

        if (currCharSpan && event.key === currCharSpan.textContent[0]) {
            textMoveSelection(socket)
        }
    }

    clickHandler = onKeyUp;
    document.addEventListener("keyup", clickHandler, false);
}

export function deinitKeyboardListeners(socket) {
    document.removeEventListener("keyup", clickHandler, false);
}


function textMoveSelection(socket) {
    const undoneTextSpan = document.querySelector(".text-block .text-undone");
    const doneTextSpan = document.querySelector(".text-block .text-done");
    const currCharSpan = document.querySelector(".text-block .curr-char");

    doneTextSpan.textContent += currCharSpan.textContent[0];
    currCharSpan.textContent = undoneTextSpan.textContent[0];
    undoneTextSpan.textContent = undoneTextSpan.textContent.substring(1);

    socket.emit("INCREASE_PROGRESS");
}