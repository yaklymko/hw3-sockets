import {roomsMap, usersArr} from "./db";
import roomHandler from "./rooms";
import gameHandler from "./game";
import {getCurrentRoomId, roomsArray} from "./utils";

// Create for tests
// roomsMap.set("testRoom", {players: [], status: 'lobby'})

function onConnect(socket) {
    const username = socket.handshake.query.username;
    if (usersArr.includes(username)) {
        socket.emit("auth_error", "user already exists");
    } else {
        usersArr.push(username)
    }

    gameHandler(socket, username, usersArr, roomsMap);
    roomHandler(socket, username, usersArr, roomsMap);

    socket.emit("UPDATE_ROOMS", roomsArray(roomsMap));
}


export default io => {
    io.on("connection", onConnect);
};
