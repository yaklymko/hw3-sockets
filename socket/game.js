import {checkIfGameFinished, checkIfPlayersReady, getCurrentRoomId} from "./utils";
import {SECONDS_TIMER_BEFORE_START_GAME} from "./config";

export default (socket, username, usersArr, roomsMap) => {
    socket.on("PLAYER_SET_READY", isReady => {
        const currRoomId = getCurrentRoomId(socket, roomsMap);
        const currRoom = roomsMap.get(currRoomId);

        currRoom.players.find(p => p.username === username).ready = isReady;

        if (currRoom.status !== "lobby") {
            return;
        }

        checkIfPlayersReady(socket, currRoomId, currRoom, SECONDS_TIMER_BEFORE_START_GAME, roomsMap);

        socket.to(currRoomId).emit("ROOM_JOINED", {name: currRoomId, ...roomsMap.get(currRoomId)});
        socket.emit("ROOM_JOINED", {name: currRoomId, ...roomsMap.get(currRoomId)});
    })

    socket.on("INCREASE_PROGRESS", () => {
        const currRoomId = getCurrentRoomId(socket, roomsMap);
        const currRoom = roomsMap.get(currRoomId);
        const currPlayer = currRoom.players.find(p => p.username === username);

        currPlayer.progress += 1;
        if (currPlayer.progress === currRoom.textLen) {
            if (!currRoom.finishers) {
                currRoom.finishers = [username];
            } else {
                currRoom.finishers.push(username);
            }
            socket.emit("UPDATE_CURR_ROOM", {name: currRoomId, ...currRoom});

            checkIfGameFinished(socket, currRoomId, currRoom, roomsMap);
        }
        socket.to(currRoomId).emit("UPDATE_CURR_ROOM", {name: currRoomId, ...currRoom});
        socket.emit("UPDATE_CURR_ROOM", {name: currRoomId, ...currRoom});
    });
}
