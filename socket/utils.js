import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME} from "./config";
import {texts} from "../data";

export function getCurrentRoomId(socket, roomsMap) {
    return Object.keys(socket.rooms).find(roomId => roomsMap.has(roomId));
}

export function roomsArray(roomsMap) {
    return Array.from(roomsMap).map(([key, value]) => ({name: key, ...value}))
        .filter(roomInfo => roomInfo.players.length < MAXIMUM_USERS_FOR_ONE_ROOM && roomInfo.status === "lobby");
}

export function checkIfPlayersReady(socket, currRoomId, currRoom, secondsBeforeStart, roomsMap) {
    if (currRoom.players && currRoom.players.every(p => p.ready)) {
        currRoom.status = "timer";

        const randomId = Math.floor(Math.random() * texts.length)
        currRoom.textLen = texts[randomId].length;

        let timer = secondsBeforeStart * 1000;

        socket.emit("START_TIMER", randomId);
        socket.to(currRoomId).emit("START_TIMER", randomId);

        socket.to(currRoomId).emit("SECONDS_BEFORE_START", timer / 1000);
        socket.emit("SECONDS_BEFORE_START", timer / 1000);

        socket.broadcast.emit("UPDATE_ROOMS", roomsArray(roomsMap));

        let preGameTimerId = setInterval(() => {
            timer -= 1000;
            socket.to(currRoomId).emit("SECONDS_BEFORE_START", timer / 1000);
            socket.emit("SECONDS_BEFORE_START", timer / 1000);

        }, 1000);

        setTimeout(() => {
            clearInterval(preGameTimerId);
            startGame(socket, currRoomId, currRoom, roomsMap);
        }, secondsBeforeStart * 1000);
    }
}


function startGame(socket, currRoomId, currRoom, roomsMap) {
    currRoom.status = "game";

    currRoom.players.forEach(p => p.progress = 0);

    socket.to(currRoomId).emit("START_GAME", {name: currRoomId, ...currRoom});
    socket.emit("START_GAME", {name: currRoomId, ...currRoom});

    let timer = SECONDS_FOR_GAME * 1000;

    let timerId = setInterval(() => {
        if (currRoom.status === "game") {
            timer -= 1000;
            socket.to(currRoomId).emit("GAME_SECONDS_LEFT", timer / 1000);
            socket.emit("GAME_SECONDS_LEFT", timer / 1000);
        } else {
            clearInterval(timerId);
            currRoom.clearGameTimer();
        }
    }, 1000);


    let gameTimerId = setTimeout(() => {
        if (currRoom.status === "game") {
            setFinishersArr(currRoom);
            finishGame(socket, currRoomId, currRoom, roomsMap);
        }
        clearInterval(timerId);

    }, SECONDS_FOR_GAME * 1000);

    currRoom.clearGameTimer = ()=>clearInterval(gameTimerId);
}

export function checkIfGameFinished(socket, currRoomId, currRoom, roomsMap) {
    if (currRoom.players.every(p1 => currRoom.finishers.includes(p1.username))) {
        finishGame(socket, currRoomId, currRoom, roomsMap);
    }
}

function setFinishersArr(currRoom) {
    currRoom.finishers = currRoom.players.sort((a, b) => b.progress - a.progress).map(p => p.username);
}

function finishGame(socket, currRoomId, currRoom, roomsMap) {
    currRoom.status = "lobby";
    currRoom.players.forEach(p => {
        p.progress = 0
        p.ready = false;
    });

    socket.emit("FINISH_GAME", currRoom.finishers);
    socket.to(currRoomId).emit("FINISH_GAME", currRoom.finishers);

    currRoom.finishers =[];

    socket.emit("UPDATE_CURR_ROOM", currRoom);
    socket.to(currRoomId).emit("UPDATE_CURR_ROOM", currRoom);

    socket.broadcast.emit("UPDATE_ROOMS", roomsArray(roomsMap))
}