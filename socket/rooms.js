import {checkIfGameFinished, checkIfPlayersReady, getCurrentRoomId, roomsArray} from "./utils";
import {SECONDS_TIMER_BEFORE_START_GAME as secondsBeforeStart} from "./config";

export default (socket, username, usersArr, roomsMap) => {

    socket.on("JOIN_ROOM", roomId => {
        const prevRoomId = getCurrentRoomId(socket, roomsMap);

        if (roomId === prevRoomId) {
            return;
        }

        if (prevRoomId) {
            socket.leave(prevRoomId);
        }

        socket.join(roomId, () => {
            const roomInfo = roomsMap.get(roomId);
            roomInfo.players.push({username: username, ready: false});
            socket.emit("ROOM_JOINED", {name: roomId, ...roomInfo});
            socket.to(roomId).emit("UPDATE_CURR_ROOM", {name: roomId, ...roomInfo});
            socket.broadcast.emit("UPDATE_ROOMS", roomsArray(roomsMap));
        })

    });

    socket.on("LEAVE_ROOM", () => {
        const currRoomId = getCurrentRoomId(socket, roomsMap)
        socket.leave(currRoomId)
        const currRoom = roomsMap.get(currRoomId);
        const playersArr = currRoom.players;
        const playersId = playersArr.findIndex(p => p.username === username);
        playersArr.splice(playersId, 1);

        if (!playersArr.length) {
            roomsMap.delete(currRoomId);
        } else {
            socket.to(currRoomId).emit("UPDATE_CURR_ROOM", {name: currRoomId, ...roomsMap.get(currRoomId)});
            checkIfPlayersReady(socket, currRoomId, roomsMap.get(currRoomId), secondsBeforeStart, roomsMap);
        }

        socket.emit("ROOM_LEFT");
        socket.emit("UPDATE_ROOMS", roomsArray(roomsMap));
        socket.broadcast.emit("UPDATE_ROOMS", roomsArray(roomsMap));

    });

    socket.on("CREATE_ROOM", roomName => {
        if (roomsMap.get(roomName)) {
            socket.emit("ERR_INVALID_ROOM_NAME", roomsArray(roomsMap));
            return;
        }
        roomsMap.set(roomName, {players: [{username: username, ready: false}], status: 'lobby'});

        socket.join(roomName, () => {
            const roomInfo = roomsMap.get(roomName);
            socket.emit("ROOM_JOINED", {name: roomName, ...roomInfo});
            socket.to(roomName).emit("UPDATE_CURR_ROOM", {name: roomName, ...roomInfo});
            socket.broadcast.emit("UPDATE_ROOMS", roomsArray(roomsMap));
        })
    });

    socket.on('disconnect', () => {
        // When disconnecting we loose all rooms from socket so need to find room id

        for (const key of roomsMap.keys()) {
            const tmpRoom = roomsMap.get(key);
            const playerId = tmpRoom.players.findIndex(p => p.username === username);
            if (playerId !== -1) {
                socket.leave(key)
                tmpRoom.players.splice(playerId, 1);

                if (!tmpRoom.players.length) {
                    roomsMap.delete(key);
                }

                socket.to(key).emit("UPDATE_CURR_ROOM", {name: key, ...roomsMap.get(key)});
                if (tmpRoom.status !== "lobby") {
                    console.log("FINSIERS")
                    console.log(tmpRoom.finishers);
                    if (tmpRoom.finishers) {
                        const index = tmpRoom.finishers.indexOf(usName => usName === username);
                        if (index !== -1) {
                            tmpRoom.finishers.splice(index, 1);
                        }
                    }
                    console.log(tmpRoom.finishers);
                    checkIfGameFinished(socket, key, tmpRoom, roomsMap);

                }
                break;
            }
        }


        usersArr.splice(usersArr.indexOf(username), 1);

        socket.broadcast.emit("UPDATE_ROOMS", roomsArray(roomsMap));
    });
}